# Bacula 9.4.2 (Postgres) for Ubuntu

Compiled from source on a Qnap LXC container running Ubuntu Xenial.

With help from http://bacula.us/compilation/, the approximate method was:

`apt-get install -y build-essential libreadline6-dev zlib1g-dev liblzo2-dev mt-st mtx postfix libacl1-dev libssl-dev postgresql-server-dev-9.6 postgresql-9.6`

...then configure:

`./configure --with-readline=/usr/include/readline --disable-conio --bindir=/usr/bin --sbindir=/usr/sbin --with-scriptdir=/etc/bacula/scripts --with-working-dir=/var/lib/bacula --with-logdir=/var/log/bacula --enable-smartalloc --with-postgresql`

...and make:

`make -j4`

...and finally 'install' into a directory so I could tar it up:

`mkdir /tmp/output`
`make PREFIXDIR=/tmp/output install`
